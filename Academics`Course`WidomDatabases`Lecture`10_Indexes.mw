* Indexes is primary mechanism to get improved performances of RDBMs.  It is persistent data structure stored in databases.  It has trade-off such as creation and updating overheads.

* Jennifer Widom prefers to use "indexes" to "indices".

* Data structures used to implement RDBM indexes include hash table and balanced tree (B and B+ tree).
** Hash table can only support $a = v$ condition;
** Balanced tree can support $a = v$, $a > v$, $v_1 < a < v_2$ conditions.

* Many RDBMs build indexes for primary keys.

* Query planning and optimization.

* Down sides of indexes:
** Overhead of maintaining indexes;
** Overhead of creating;
** Indexes takes extra space.

* Benefit of indexes depends on
** size of table;
** data distribution;
** query vs. update load.


* Physical design advisors uses database statistics and workflow and give useful indexes as output.

* Query optimizer uses 1) database statistics, 2) input or update query, 3) indexes to determine best execution plan.


* SQL statements for creating indexes:
** <code>CREATE INDEX index_name ON T(A);</code>
** <code>CREATE INDEX index_name ON T(A1, A2, ...);</code>
** <code>CREATE UNIQUE INDEX index_name ON T(A);</code>
** <code>DROP INDEX index_name;</code>

== Tags ==

[[Category:Academics]]
[[Category:ComputerScienceAndEngineering]]
[[Category:Computing]]
[[Category:Course]]
[[Category:Database]]
[[Category:Programming]]
[[Category:WidomDatabases]]
