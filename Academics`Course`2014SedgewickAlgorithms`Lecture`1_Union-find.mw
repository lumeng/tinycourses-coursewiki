== Dynamic connectivity problem ==

* union

* find/connected query



=== Modeling the connections ===

* Connectivity* is an [http://en.wikipedia.org/wiki/Equivalence_relation equivalence relation] with the following properties:
** Reflexive: object p is connected to itself;
** Symmetric: if object p is connected to q, then q is connected to p;
** Transitive: if object p is connected to q, and q is cnnected to r, then p is connected to r.


== [http://en.wikipedia.org/wiki/Disjoint-set_data_structure Disjoint-set data structure] ==

== Union-find algorithm ==

=== Union-find algorithm with quick find ===


* "eager approach".
* Data structure:
** Use an ID array <code>int id[]</code>.
** Two nodes p and q are connected if <code>id[p] == id[q]</code>.
* Find operation: simply check <code>id[p] == id[q]</code>, which is fast.
* Union operation: check and update all <code>id[q]</code>, whose complexity is <math>\mathcal O \left( n^2 \right)</math>.
* Quadratic algorithms do not scale. 
** Current CPU's clock rate is ~GHz and memory is ~GB, which means roughly that all words in memory is accessed in ~1 sec.
** Quick find algorithm with "eager approach" takes <math>\left(10^9\right)^2 = 10^{18}</math> accesses and 32 years.

=== Union-find algorithm with quick union ===

* "Lazy approach": "try not to do work until needed."
* Data structure: use <code>root[]</code> array to store root node (NOT parent node) of each node.
* <code>find(p, q)</code>: check if <code>root[p] == root[q]</code>.
* <code>union(p, q)</code>: update root <code>root[p] = root[q]</code>.
* Drawback is that in the worst case <code>find(p, q)</code> requires <math>\mathcal O \left( n \right)</math> time.  The worst case is when all the nodes form a tall tree.

=== Union-find algorithm with weighted union ===

* Improve quick union by avoiding tall trees.
* Use <code>size[]</code> to store size of component trees.
* Idea: avoid attaching large trees under the root of a smaller tree, which makes the tree taller, rather, do the opposite.
* The average distance from a node to its root node is small, which makes <code>find(p, q)</code> less expensive on average.
* Depth of a node is at the most <math>\log_2 n</math>
** (Qualitative) proof: Each union causes the smaller tree's height to increase by 1; The size of the resulting tree for union always at least doubles; So the tree size at the most doubles <math>\log_2 n</math> times which causes <math>\log_2 n</math> increase to the height.
* Complexity: both find and union takes <math>\log n</math> operations.

=== Union-find algorithm with quick union and path compression ===

=== Union-find algorithm with weighted quick union and path compression ===
* The union-find algorithm with weighted quick union and path compression achieves the fastest complexity <math>N + M \alpha \left(M, N\right)</math> for the dynamic connectivity problem.  In the worst case, the complexity for M union-find operations on M objects is <math>N + M \log^* N</math>.
* Improve <code>find(p, q)</code> Set root of all examined nodes to the true root.
* Any sequence of M union find operations on N objects makes <math>\leq c \left( N + M \log^* N \right)</math> array accesses.
** Analysis can be improved to <math>N + M \alpha \left(M, N\right)</math>
** [https://en.wikipedia.org/wiki/Proof_of_O(log*n)_time_complexity_of_union%E2%80%93find Proof of O(log*n) time complexity of union–find]
* [https://en.wikipedia.org/wiki/Iterated_logarithm Iterated logarithm] <math>\log_2^* N</math>;  <math>\log_2^* N</math> for realistic input is smaller than 5.  <math>\log_2^* 2^{2^{16}} = \log_2^* 2^{65536} = 5</math>
* [https://en.wikipedia.org/wiki/Inverse_Ackermann_function#Inverse Inverse Ackermann function] <math>\alpha \left(M, N\right)</math> is probably the slowest growing function that is normally used in representing algorithmic complexities.
* It can be proved there does not exist linear complexity algorithm for the dynamic connectivity problem.

=== Worst case performance of the variants of union find algorithm ===

TODO

== Application of union-find algorithm ==

* Percolation: find threshold density of empty sites when phase transition happens occurs by simulation, where percolation path is identified by union-find algorithm.

[[Category:Course]]

[[Category:Academics]]

[[Category:Lecture]]

[[Category:ComputerScienceAndEngineering]]

[[Category:Algorithm]]

[[Category:2014SedgewickAlgorithms]]
