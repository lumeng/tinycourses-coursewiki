{|width="100%"
|-
|[[Academics`Course`Probability and statistics`Enchiridion`Random variables|← Previous: Random variables]]
|
| [[Academics`Course`Probability and statistics`Enchiridion|Index]]
|
| style="text-align:right;"|[[Academics`Course`Probability and statistics`Enchiridion`Variance|Next: Variance →]]
|}{{LaTeXPreamble}}
= Expectation =

== Expectation definition ==

* $\E{X} = \mu_X = \displaystyle \int x \mathop{}\!\mathrm{d} F \left(x\right) =
    \begin{cases}
      \displaystyle\sum_x x \: f_X(x) & \text{X discrete} \\\\
      \displaystyle\int x \: f_X(x)\dx & \text{X continuous}
    \end{cases}$
** $\E{X}$ exists if $\int \left\vert x \right\vert \dfx < \infty$	
** Typical pathological example where $\E{X}$ cannot be defined: [http://en.wikipedia.org/wiki/Cauchy_distribution Cauchy distribution] has a thick tail which means extreme observations occur frequent enough that the average never converge.

* $k^{\mathrm{th}}$ [https://en.wikipedia.org/wiki/Moment_(mathematics) moment] is $\E{X^k}$ if $\E{\left \vert X \right \vert^k} < \infty.$

* $k^{\mathrm{th}}$ [https://en.wikipedia.org/wiki/Central_moment central moment] is $\E{\left(X - \mu \right)^k}$ if $\E{\left \vert X - \mu \right \vert^k} < \infty.$

* CDF is expectation of the [https://en.wikipedia.org/wiki/Indicator_function indicator function] $\mathbf{1}_{\{X \leq x\}}$:

$$F_X(x) = \E{\mathbf{1}_{\{X \leq x\}}}$$

* [https://en.wikipedia.org/wiki/Characteristic_function_(probability_theory) Characterisitic function] $\varphi_X(t)$ is expectation of the Fourier transformation multiplicative factor $e^{i t X}$:

$$\varphi_X(x) = \E{e^{i t X}}$$


== Properties of expectations ==

* $\Pr{X=c}=1 \imp \E{X} = c$

* $\E{cX} = c\,\E{X}$

* $\E{X+Y} = \E{X}+\E{Y}$

* $\E{XY} = \displaystyle\int_{X,Y} x \, y \, f_{X,Y}(x,y)\dfx\dfy$

* $\E{X^2} = \sigma_X^2 + \left(\E{X}\right)^2 = \sigma_X^2 + \mu_X^2$

* Iff $X \ind Y$, $\E{X Y} = \E{X} \E{Y}$

* $\E{\transform(Y)} \neq \transform(\E{X})$, if $\varphi$ is convex,  [http://en.wikipedia.org/wiki/Jensen's_inequality Jensen's inequality] $\transform(\E{X}) \leq \E{\transform(Y)}$
** A special case of Jensen's inequality for $\varphi: x \mapsto x^2$, $\E{X^2} = \V{X} + (\E{X})^2 = \sigma_X^2 + \mu_X^2 \geq (\E{X})^2$. $\E{X^2} = \mu_X^2$ if $\sigma_X^2=0$ which for discrete distributions means $X = const.$

* $\Pr{X\ge Y} = 0 \imp \E{X}\ge\E{Y} \wedge \Pr{X=Y}=1 \imp \E{X}=\E{Y}$

* $\Pr{\lvert Y\rvert\le c} = 1 \imp \E{Y}<\infty \wedge \lvert\E{X}\rvert\le c$ (?)

* $\E{X} = \displaystyle\sum_{x=1}^\infty \Pr{X\ge x}$

* For a random vector $\mathbf{X}$ (column vector) and a constant column vector of same dimension $\mathbf{a}$, $\E{\mathbf{a}^T \mathbf{X}} = \mathbf{a}^T \E{\mathbf{X}} = \mathbf{a}^T \mathbf{\mu}$. (c.f. Wasserman ''All of Statistics'' pp. 54)

* For a random vector $\mathbf{X}$ (column vector) and a constant matrix $A$, $\E{A \mathbf{X}} = A \E{\mathbf{X}} = A \mathbf{\mu}$. (c.f. Wasserman ''All of Statistics'' pp. 54)


== [http://en.wikipedia.org/wiki/Law_of_the_unconscious_statistician The law of the unconscious statistician] ==

A.k.a. the rule of the Lazy Statistician.

$$\mathbb{E}\left(Z\right) = \int \varphi \left( x \right)
\d F_X \left(x\right)$$

$$\mathbb{E}\left(Z\right) = \int \varphi \left( x_1, \cdots, x_n \right)
\d F_{X_1, \cdots, X_n} \left(x, \cdots, x_n\right)$$

(c.f.
[http://en.wikipedia.org/wiki/Riemann%E2%80%93Stieltjes_integral#Application_to_probability_theory application of Riemann-Stieltjes integral to probability theory].)


Probability is a special case of expectation:

$$\mathbb{E}\left(I_A(x)\right) = \int I_A(x) \mathop{}\!\mathrm{d}
F_X \left(x\right)
= \int_A \mathop{}\!\mathrm{d} F_X \left(x\right) = \mathbb{P}\left(X \in A\right),$$

where

$
I_A(x) =
\begin{cases}
\displaystyle 0 & \quad x \in A \\\\
\displaystyle 1 & \quad x \not \in A
\end{cases}
$


== Sample mean ==
$$\samplemean = \frac{1}{n}\sum_{i=1}^n X_i$$

== Conditional expectation ==

* $\E{Y\giv X=x} = \displaystyle\int y \: f(y\giv x)\dy$
* $\E{X\giv Y}$ is a random variable whose value is $\E{X\giv Y = y}$ when $Y = y$.
* The rule of iterated expectations: $\E{X} = \E{\E{X\giv Y}}$, $\E{\transform\left(X, Y\right) | X} = \E{\transform\left(X, Y\right)}$.
* $E[\transform(X,Y)\giv X=x] = \displaystyle\int_{-\infty}^\infty \transform(x,y) f_{Y|X}(y\giv x)\dx$
* $\E{\transform(Y,Z)\giv X=x} = \displaystyle\int_{-\infty}^\infty\transform(y,z) f_{(Y,Z)|X}(y,z\giv x)\,dy\,dz$
* $\E{Y+Z\giv X} = \E{Y\giv X} + \E{Z\giv X}$
* $\E{\transform(X)Y\giv X} = \transform(X)\E{Y\giv X}$
* $\E{Y\giv X} = c \imp \cov{X,Y}=0$


[[Category:Academics]]
[[Category:Probability]]
[[Category:Statistics]]
[[Category:Note]]
