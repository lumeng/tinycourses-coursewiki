== Introduction to transactions ==

* Motivation for transations:
*# consistency between concurrent SQL operations.
*# all-or-nothing execution, regardless of failures

* A transaction is a sequence of one or more SQL operations treated as a unit.


== Transaction properties ==

* The [https://en.wikipedia.org/wiki/ACID ACID] property of transactions:
** [https://en.wikipedia.org/wiki/Atomicity_(database_systems) Atomicity]: all statements of a transaction is a unit which is executed in "all-or-nothing" fashion.
** [https://en.wikipedia.org/wiki/Consistency_(database_systems) Consistency]: any transactions started in the future necessarily see the effects of other transactions committed in the past, and the database constraints are not violated after transaction commits.
** [https://en.wikipedia.org/wiki/Isolation_(database_systems) Isolation]: 
** [https://en.wikipedia.org/wiki/Durability_(database_systems) Durability]: after <code>COMMIT</code> of a transaction, its effect in a database is durable, ie. will survive permenantly even after system failures.


* transaction rollback

== Isolation levels ==

* [https://en.wikipedia.org/wiki/Serializability Sieralizability]: if multiple transactions are issued concurrently, they may be interleaved, but executation must be equivalent to some serial, i.e. sequential without overlapping in time, order of all transactions, For example, if transactions <code>T1</code> and <code>T2</code> are issued concurrently, the execution is equivalent to either "<code>T1, T2;</code>" or "<code>T2; T1;</code>". In contrast, the transactions are not allowed to be overlapping in time.

{|class="wikitable"
|
|dirty reads
|nonrepeatable reads
|phantom rows
|-
|READ UNCOMMITTED
|✓
|✓
|✓
|-
|READ COMMITTED
|X
|✓
|✓
|-
|REPEATABLE READ
|X
|X
|✓
|-
|SERIALIZABLE
|X
|X
|X
|-
|}


* The default isolation level of Oracle database and MySQL database is <code>REPEATABLE READ</code>.


[[Category:Academics]]
[[Category:ComputerScienceAndEngineering]]
[[Category:Computing]]
[[Category:Course]]
[[Category:Database]]
[[Category:Programming]]
[[Category:WidomDatabases]]
