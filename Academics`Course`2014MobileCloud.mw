== Metadata == 

* course title: Programming Mobile Applications for Android Handheld Systems
* time: Jan. 29, 2014 - Apr. 6, 2014
* course Web site: https://class.coursera.org/mobilecloud-001/
* course wiki: 
* instructor: Dr. C. Jules White, Dr. Douglas C. Schmidt
* tags: Computing, Academics, ComputerScienceAndEngineering, Android, Course, 2014MobileCloud, Library, Reference, Programming, Java, SpringFramework
* Source code: https://github.com/juleswhite/mobilecloud-14


== [[Academics`Course`2014MobileCloud`Lecture|Lectures]] ==

== [[Academics`Course`2014MobileCloud`Assignment|Assignments]] ==

== [[Academics`Course`2014MobileCloud`Book|Books]] ==



== References ==

=== General info ===

* http://developer.android.com
* http://android-developers.blogspot.com

=== Q&A ===

* https://groups.google.com/group/android-developers
* http://stackoverflow.com/questions/tagged/android

=== Networking ===


=== Documentation ===


* w3c.org documentation "HTTP - Hypertext Transfer Protocol", http://www.w3.org/Protocols/

* [https://en.wikipedia.org/wiki/Representational_state_transfer#Applied_to_web_services Wikipedia article "Representational state transfer"]

* [http://docs.oracle.com/javaee/7/tutorial/doc/servlets.htm oracle.com The Java EE Tutorial, 17 Java Servlet Technology]

* [https://developers.google.com/appengine/docs/java/gettingstarted/introduction Google App Engine tutorial on using Java supported by Java Servlet on Google App Engine]

＊ [http://www.oracle.com/technetwork/java/javaee/tech/persistence-jsp-140049.html Java Persistence API]

* [http://docs.spring.io Spring Framework documentation]
** Javadoc API: http://docs.spring.io/spring/docs/current/javadoc-api/
*** <code>Class ResponseEntity<T></code>: http://docs.spring.io/spring-framework/docs/current/javadoc-api/org/springframework/http/ResponseEntity.html
** Spring Framework guides: http://spring.io/guides#gs
** Spring Framework reference: http://docs.spring.io/spring/docs/current/spring-framework-reference/
** Spring Framework reference -> 16. Web MVC framework: http://docs.spring.io/spring/docs/current/spring-framework-reference/html/mvc.html
** Exception Handling in Spring MVC: http://spring.io/blog/2013/11/01/exception-handling-in-spring-mvc

=== Assignments ===


=== Other ===

* James Marshall, HTTP Made Really Easy, A Practical Guide to Writing Clients and Servers, http://www.jmarshall.com/easy/http/

* Roy Fielding's dissertation on REST: http://www.ics.uci.edu/~fielding/pubs/dissertation/rest_arch_style.htm

== Tools ==

* HTTP request generator: http://www.hurl.it/
* Postman for Chrome, a browser extensions to manually construct HTTP requests, https://chrome.google.com/webstore/detail/postman-rest-client/fdmmgilgnpjigdojojpjoooidkmcomcm?hl=en
* RestClient for Firefox, a browser extensions to manually construct HTTP requests, https://addons.mozilla.org/en-US/firefox/addon/restclient/

* Jackson (http://jackson.codehaus.org/) JSON processor.

* Spring Framework (http://projects.spring.io/spring-framework/)

* Retrofit (http://square.github.io/retrofit/): a framework for truning REST-ful API into a Java interface.

* Google App Engine (https://developers.google.com/appengine/)

* The ADAPTIVE Communication Environment (ACE) http://www.dre.vanderbilt.edu/~schmidt/ACE.html

* The JAWS Adaptive Web Server (JAWS) (http://www.dre.vanderbilt.edu/JAWS/)


== Tags ==

[[Category:Course]]
[[Category:Academics]]
[[Category:ComputerScienceAndEngineering]]
[[Category:Android]]
[[Category:2014MobileCloud]]
